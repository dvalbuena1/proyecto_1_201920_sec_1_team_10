package test.model.logic;


import org.junit.Before;
import org.junit.Test;

import model.data_structures.ListaEncadenada;
import model.logic.Ordenamientos;

import static org.junit.Assert.*;

import java.util.Comparator;

public class OrdenadorTest
{

	   
	/**
	 * Ordenador que se probar�
	 */
	private Ordenamientos<Integer> ordenador;

	/**
	 * lista para realizar las pruebas
	 */
	private ListaEncadenada<Integer> lista1;
	
	

	/**
	 * Prepara el escenario de pruebas
	 */
	@Before
	public void setUEscenario0()
	{
		ordenador = new Ordenamientos<Integer>();
		lista1 = new ListaEncadenada<Integer>();
	}
	
	public void setUpEscenario1()
	{		
		lista1.add(new Integer(10));
		lista1.add(new Integer(-25));
		lista1.add(new Integer(13));
		lista1.add(new Integer(45));
		lista1.add(new Integer(-1));
		lista1.add(new Integer(-8));
		lista1.add(new Integer(8));
	}
	
	public void setUpEscenario2()
	{
		for (int i = 0; i < 6; i++) 
		{
			lista1.add(new Integer(i));
		}
		
	}
	
	public void setUpEscenario3()
	{
		for (int i = 7; i >0 ; i--) 
		{
			lista1.add(new Integer(i));
		}
		
	}

	/**
	 * Prueba 1: Se encarga de verificar el m�todo ordenar de la clase.<br>
	 * <b> M�todos a probar: </b> <br>
	 * ordenar<br>
	 * <b> Casos de prueba:</b><br>
	 * 1. Ordena correctamente por todos los algoritmos ascendentemente.<br>
	 * 2. Ordena correctamente por todos los algoritmos descendentemente.<br>
	 */
	@Test
	public void testOrdenarDesorden()
	{
		setUpEscenario1();  
		
		
		
		Comparable<Integer>[] arreglo1 = lista1.toArray();
		
		ordenador.sortQuick(arreglo1, new ComparadorNumeros(), false);
		for (int i = 0; i < arreglo1.length-1; i++) 
		{
			assertTrue("No ordeno la lista",(Integer) arreglo1[i] < (Integer) arreglo1[i+1]);
		}
		
		
	}
	
	@Test
	public void testOrdenarOrden()
	{
		setUpEscenario2();  
		
		
		
		Comparable<Integer>[] arreglo1 = lista1.toArray();
		
		ordenador.sortQuick(arreglo1, new ComparadorNumeros(), false);
		for (int i = 0; i < arreglo1.length-1; i++) 
		{
			assertTrue("No ordeno la lista",(Integer) arreglo1[i] < (Integer) arreglo1[i+1]);
		}
		
		
	}
	
	@Test
	public void testOrdenarReves() 
	{
		setUpEscenario3();  
		
		
		
		Comparable<Integer>[] arreglo1 = lista1.toArray();
		
		ordenador.sortQuick(arreglo1, new ComparadorNumeros(), false);
		for (int i = 0; i < arreglo1.length-1; i++) 
		{
			assertTrue("No ordeno la lista", (Integer) arreglo1[i] < (Integer) arreglo1[i+1] );
		}
		
	}	
	/**
	 * clase de comparaci�n para probar el ordenador
	 *
	 */
	private class ComparadorNumeros implements Comparator<Integer>
	{

		/**
		 * Compara dos n�meros
		 * @return	1 si el n�mero o1 es mayor al n�mero o2 
		 * 			-1 si el n�mero o1 es menor al n�mero o2
		 * 			0 si el n�mero o1 es igual al n�mero o2
		 */
		public int compare(Integer o1, Integer o2) {
			int comparar = o1-o2;
			if(comparar > 0)
			{
				return 1;
			}
			else if( comparar < 0 )
			{
				return -1;
			}
			else
			{
				return 0;			
			}
		}

	}

	
}
