package view;

import java.util.Iterator;

import model.data_structures.ListaEncadenada;
import model.logic.MVCModelo;
import model.logic.ViajeHora;
import model.logic.ViajeMes;
import model.logic.ViajeSemana;

public class MVCView 
{
	/**
	 * Metodo constructor
	 */
	public MVCView()
	{

	}

	public void printMenu()  
	{
		System.out.println("1. Cargar archivo");
		System.out.println("2. Consultar el tiempo promedio de viaje y su desviaci�n est�ndar de los viajes, dada una zona de origen, zona destino y mes/dia de la semana/hora del dia");
		System.out.println("3. Consultar la informacion de los viajes con mayor tiempo promedio para un mes/dia de la semana/hora del dia");
		System.out.println("4. Comparar los tiempos promedios de los viajes para una zona y un rango de zonas, para un mes o un dia de la semana");
		System.out.println("5. Generar grafica ASCII de los tiempos promedios dado la zona origen y zona destino");
		System.out.println("6. Exit");
		System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return");
	}

	public void printMenuTPromedioDEstantar()
	{
		System.out.println("1. Dado un mes");
		System.out.println("2. Dado un dia de la semana");
		System.out.println("3. Dado una hora inicial y final");
	}

	public void printMenuMayorTPromedio()
	{
		System.out.println("1. Dado un mes");
		System.out.println("2. Dado un dia de la semana");
		System.out.println("3. Dado una hora del dia");
	}
	public void printMenuCompararTiempo()
	{
		System.out.println("1. Dado un mes");
		System.out.println("2. Dado un dia de la semana");
	}
	//R1
	public void printMessageCargar(MVCModelo modelo) 
	{
		System.out.println("Total de viajes por mens: " + modelo.sizeM());
		System.out.println("Total de viajes por semana: " + modelo.sizeS());
		System.out.println("Total de viajes por hora: " + modelo.sizeH());
		System.out.println("Menor identificador: " + modelo.darMenorIdentificador());
		System.out.println("mayor identificador: " + modelo.darMayorIdentificador());
	}

	public void printMessageConsultaR2(ListaEncadenada<ViajeMes> viajes) 
	{
		if(viajes.size() != 0)
		{
			int i = 1;
			Iterator<ViajeMes> it = viajes.iterator();
			while(it.hasNext())
			{
				ViajeMes viaje = it.next();
				double tPromedio = viaje.darTiempoPromedio();
				double dEstandar = viaje.darDesviacionEstandar();
				System.out.print("Viaje numero: " + i);
				System.out.print("   Tiempo promedio: " + tPromedio);
				System.out.println("   Desviacion estandar: " + dEstandar);
				i++;
			}
		}
		else
			System.out.println("No se encontraron viajes");
	}

	public void printMessageConsultaR3(ViajeMes[] viajes) 
	{			
		if(viajes.length!=0)
		{
			for (int i = 0; i < viajes.length; i++) 
			{
				ViajeMes viaje= viajes[i];
				int zonaO= viaje.darIdOrigen();
				int zonaD= viaje.darIdDestino();
				double tiempo= viaje.darTiempoPromedio();
				double desviacion= viaje.darDesviacionEstandar();
				System.out.print("Zona de Origen: "+ zonaO);
				System.out.print("    Zona de Destino: "+ zonaD);
				System.out.print("    Tiempo Promedio de Viaje: "+ tiempo);
				System.out.println("    Desviacion Estandar: "+ desviacion); 					
			}				
		}
		else
			System.out.println("No se encontraron viajes");
	}

	public void printMessageConsultaR4(Comparable<ViajeMes>[][] arr) 
	{

			
		Comparable<ViajeMes>[] viajes1 = arr[0];
		Comparable<ViajeMes>[] viajes2 = arr[1];
			
			for (int i = 0; i < viajes1.length; i++) 
			{
				ViajeMes viaje= (ViajeMes) viajes1[i];
				double tiempo= viaje.darTiempoPromedio();
				int zonaO= viaje.darIdOrigen();
				int zonaD = viaje.darIdDestino();
				
				for (int j = 0; j < viajes2.length; j++) 
				{
					ViajeMes viaje2= (ViajeMes) viajes2[j];
					double tiempo2= viaje2.darTiempoPromedio();
					int zonaO2= viaje2.darIdOrigen();
					int zonaD2 = viaje2.darIdDestino();
					if(viajes1.length==0 )
					{
						System.out.println("No hay viajes de: "+ zonaO+ " a "+ zonaD+ " vs "+ tiempo2+ " de: "+ zonaO2+ " a "+ zonaD2);

						if(viajes2.length==0)
						{
							System.out.println("No hay viajes de: "+ zonaO+ " a "+ zonaD+ " vs "+" No hay viajes de "+ zonaO2+ " a "+ zonaD2);
							break;
						}
					}
					if(viajes2.length==0)
					{
						System.out.println(tiempo+ " de "+ zonaO+ " a "+ zonaD+ " vs "+" No hay viajes de "+ zonaO2+ " a "+ zonaD2);
						break;
					}
					System.out.println(tiempo+ " de: "+ zonaO+ " a "+ zonaD+ " vs "+ tiempo2+ " de: "+ zonaO2+ " a "+ zonaD2);
					
				}				
			}			
	}

	public void printMessageConsultaR5(ListaEncadenada<ViajeSemana> viajes) 
	{
		if(viajes.size() != 0)
		{
			int i = 1;
			Iterator<ViajeSemana> it = viajes.iterator();
			while(it.hasNext())
			{
				ViajeSemana viaje = it.next();
				double tPromedio = viaje.darTiempoPromedio();
				double dEstandar = viaje.darDesviacionEstandar();
				System.out.print("Viaje numero: " + i);
				System.out.print("   Tiempo promedio: " + tPromedio);
				System.out.println("   Desviacion estandar: " + dEstandar);
				i++;
			}
		}
		else
			System.out.println("No se encontraron viajes");		
	}

	public void printMessageConsultaR6(ViajeSemana[] viajes) 
	{

		if(viajes.length!=0)
		{
			for (int i = 0; i < viajes.length; i++) 
			{
				ViajeSemana viaje= viajes[i];
				int zonaO= viaje.darIdOrigen();
				int zonaD= viaje.darIdDestino();
				double tiempo= viaje.darTiempoPromedio();
				double desviacion= viaje.darDesviacionEstandar();
				System.out.print("Zona de Origen: "+ zonaO);
				System.out.print("    Zona de Destino: "+ zonaD);
				System.out.print("    Tiempo Promedio de Viaje: "+ tiempo);
				System.out.println("    Desviacion Estandar: "+ desviacion);					
			}			
		}
		else
			System.out.println("No se encontraron viajes");			
	}

	public void printMessageConsultaR7(Comparable<ViajeSemana>[][] arr) 
	{
		Comparable<ViajeSemana>[] viajes1=arr[0];
		Comparable<ViajeSemana>[] viajes2=arr[1];
		
		for (int i = 0; i < viajes1.length; i++) 
		{
			ViajeSemana viaje= (ViajeSemana) viajes1[i];
			double tiempo= viaje.darTiempoPromedio();
			int zonaO= viaje.darIdOrigen();
			int zonaD = viaje.darIdDestino();
			
			for (int j = 0; j < viajes2.length; j++)   
			{
				ViajeSemana viaje2= (ViajeSemana) viajes2[j];
				double tiempo2= viaje2.darTiempoPromedio();
				int zonaO2= viaje2.darIdOrigen();
				int zonaD2 = viaje2.darIdDestino();
				if(viajes1.length==0 )
				{
					System.out.println("No hay viajes de: "+ zonaO+ " a "+ zonaD+ " vs "+ tiempo2+ " de: "+ zonaO2+ " a "+ zonaD2);

					if(viajes2.length==0)
					{
						System.out.println("No hay viajes de: "+ zonaO+ " a "+ zonaD+ " vs "+" No hay viajes de "+ zonaO2+ " a "+ zonaD2);
						break;
					}
				}
				if(viajes2.length==0)
				{
					System.out.println(tiempo+ " de "+ zonaO+ " a "+ zonaD+ " vs "+" No hay viajes de "+ zonaO2+ " a "+ zonaD2);
					break;
				}
				System.out.println(tiempo+ " de: "+ zonaO+ " a "+ zonaD+ " vs "+ tiempo2+ " de: "+ zonaO2+ " a "+ zonaD2);
				
			}
			
			
		}
	}

	public void printMessageConsultaR8(ListaEncadenada<ViajeHora> viajes) 
	{

		if(viajes.size() != 0)
		{
			int i = 1;
			Iterator<ViajeHora> it = viajes.iterator();
			while(it.hasNext())
			{
				ViajeHora viaje = it.next();
				double tPromedio = viaje.darTiempoPromedio();
				double dEstandar = viaje.darDesviacionEstandar();
				System.out.print("Viaje numero: " + i);
				System.out.print("   Tiempo promedio: " + tPromedio);
				System.out.println("   Desviacion estandar: " + dEstandar);
				i++;
			}
		}
		else
			System.out.println("No se encontraron viajes");	


	}

	public void printMessageConsultaR9(ViajeHora[] viajes) 
	{
		if(viajes.length!=0)
		{
			for (int i = 0; i < viajes.length; i++) 
			{
				ViajeHora viaje= viajes[i];
				int zonaO= viaje.darIdOrigen();
				int zonaD= viaje.darIdDestino();
				double tiempo= viaje.darTiempoPromedio();
				double desviacion= viaje.darDesviacionEstandar();
				System.out.print("Zona de Origen: "+ zonaO);
				System.out.print("    Zona de Destino: "+ zonaD);
				System.out.print("    Tiempo Promedio de Viaje: "+ tiempo);
				System.out.println("    Desviacion Estandar: "+ desviacion);					
			}		
		}
		else
			System.out.println("No se encontraron viajes");			
	}

	public void printMessageConsultaR10(int[] datosTPromedio, int trim, int zonaO, int zonaD) 
	{
		System.out.println("Aproximaci�n en minutos de viajes entre zona origen y zona destino.");
		System.out.println("Trimestre " + trim + " del 2018 detallado por cada hora del d�a");
		System.out.println("Zona Origen: " + zonaO);
		System.out.println("Zona Destino: " + zonaD);
		System.out.println("Hora| # de minutos");
		
		for (int i = 0; i < datosTPromedio.length; i++) 
		{
			System.out.print((i >= 10?"":"0") + i + " | ");
			int cantidad = datosTPromedio[i];
			if(cantidad != 0)
			{
				int j = 0;			
				while(j < cantidad)
				{
					System.out.print("*");				
					j++;
				}
			}
			else
				System.out.print("hora sin viajes");
			System.out.println();
		}
		

	}
}
