package controller;

import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

import model.data_structures.ListaEncadenada;
import model.logic.MVCModelo;
import model.logic.ViajeAbstracto;
import model.logic.ViajeHora;
import model.logic.ViajeMes;
import model.logic.ViajeSemana;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;

		while( !fin )
		{
			view.printMenu();

			int option = lector.nextInt();
			switch(option)
			{
				case 1:
					try 
				    {
						System.out.println("Ingresar el trimestre a cargar: ");
						int trimestre = lector.nextInt();
						if(4-trimestre < 0 || trimestre == 0)
						{
							System.out.println("Trimestre erroneo");
							break;
						}
						modelo.asignarTrismestre(trimestre);
						modelo.cargarArchivo("./data/bogota-cadastral-2018-"+ trimestre +"-All-HourlyAggregate.csv", 1);
						modelo.cargarArchivo("./data/bogota-cadastral-2018-"+ trimestre +"-All-MonthlyAggregate.csv", 2);
						modelo.cargarArchivo("./data/bogota-cadastral-2018-"+ trimestre +"-WeeklyAggregate.csv", 3);

						System.out.println("Archivo Cargado");							
						view.printMessageCargar(modelo);
				    }
				    catch (IOException e) 
				    { 
				    	System.out.println("Se genero un error: " + e.getMessage() );						
				    }				    
					break;

				case 2:	
					System.out.println("Ingrese la zona origen:");
					int zonaO = lector.nextInt();
					System.out.println("Ingrese la zona destino:");
					int zonaD = lector.nextInt();
					view.printMenuTPromedioDEstantar();
					int opcionConsulta1 = lector.nextInt();
					ListaEncadenada viajes;
					switch (opcionConsulta1) 
					{
						case 1:							
							System.out.println("Ingrese el mes:");
							int mes = lector.nextInt();
							viajes = modelo.darTiempoDesviacionMes(zonaO, zonaD, mes);
							view.printMessageConsultaR2(viajes);
							break;
						case 2:
							System.out.println("Ingrese el dia de la semana:");
							int dia = lector.nextInt();
							viajes = modelo.darTiempoDesviacionSemana(zonaO, zonaD, dia);
							view.printMessageConsultaR5(viajes);
							break;							
						case 3:
							System.out.println("Ingrese hora inicial:");
							int horaI = lector.nextInt();
							System.out.println("Ingrese hora final:");
							int horaF = lector.nextInt();
							
							if((horaF - horaI) < 0)
							{
								System.out.println("La hora final debe ser mayor a la inicial!");
								break;
						    }
							
							viajes = modelo.darViajesFranjaHoraria(horaI, horaF, zonaO, zonaD);
							view.printMessageConsultaR8(viajes);							
							break;

						default:
							System.out.println("--------- \n Opcion Invalida !! \n---------");
							break;
					}					
					break;

				case 3:	
					System.out.println("Ingrese la cantidad de viajes a mostrar");
					int nViajes = lector.nextInt();
					view.printMenuMayorTPromedio();;
					int opcionConsulta2 = lector.nextInt();
					ViajeAbstracto[] viajes2;
					switch (opcionConsulta2)
					{
						case 1: 
							System.out.println("Ingrese el mes:");
							int mes = lector.nextInt();
							viajes2 = modelo.darNumeroViajesMesMayorPromedio(mes, nViajes);
							view.printMessageConsultaR3((ViajeMes[]) viajes2);
							break;
						case 2:
							System.out.println("Ingrese el dia de la semana:");
							int dia = lector.nextInt();
							viajes2 = modelo.darNumeroViajesSemanaMayorPromedio(dia, nViajes);
							view.printMessageConsultaR6((ViajeSemana[]) viajes2);
							break;
						case 3:
							System.out.println("Ingrese hora del dia:");
							int hora = lector.nextInt();
							viajes2 = modelo.darNumeroViajesHoraMayorPromedio(hora, nViajes);
							view.printMessageConsultaR9((ViajeHora[]) viajes2);
							break;					
						
						default: 
							System.out.println("--------- \n Opcion Invalida !! \n---------");
							break;					
					}
					break;	
				case 4:
					view.printMenuCompararTiempo();
					int opcion = lector.nextInt();
					switch(opcion)
					{
						case 1:
							System.out.println("Ingrese el mes:");
							int mes = lector.nextInt();
							System.out.println("Ingrese la zona a comparar:");
							int zonaC= lector.nextInt();
							System.out.println("Ingrese la menor zona del rango:");
							int zonaMenor=lector.nextInt();
							System.out.println("Ingrese la mayor zona del rango:");
							int zonaMayor=lector.nextInt();
							Comparable<ViajeMes>[][] arr = modelo.darComparacionTiempoPromedioMes(mes, zonaMenor, zonaMayor, zonaC);
							view.printMessageConsultaR4(arr);
							break;
						case 2:
							System.out.println("Ingrese el dia de la semana:");
							int dia = lector.nextInt();
							System.out.println("Ingrese la zona a comparar:");
							int zonaC2= lector.nextInt();
							System.out.println("Ingrese la menor zona del rango:");
							int zonaMenor2=lector.nextInt();
							System.out.println("Ingrese la mayor zona del rango:");
							int zonaMayor2=lector.nextInt();
							Comparable<ViajeSemana>[][] arr2= modelo.darComparacionTiempoPromedioSemana(dia, zonaMenor2, zonaMayor2, zonaC2);
							view.printMessageConsultaR7(arr2);	
							break;
					}
					break;
				case 5:
					System.out.println("Ingrese la zona origen:");
					int zonaOr = lector.nextInt();
					System.out.println("Ingrese la zona destino:");
					int zonaDe = lector.nextInt();
					int[] datos = modelo.darListaParaGraficaASCII(zonaOr, zonaDe);
					view.printMessageConsultaR10(datos, modelo.darTrimestre(),zonaOr,zonaDe);	
					break;
				case 6: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
