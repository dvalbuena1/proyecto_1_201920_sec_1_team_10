package model.logic;

import java.util.Comparator;

public class ComparadorIdentificadorZonaDestino implements Comparator<ViajeAbstracto>
{

	@Override
	public int compare(ViajeAbstracto v1, ViajeAbstracto v2) 
	{
		if(v1.darIdDestino() < v2.darIdDestino())
			return -1;
		else if(v1.darIdDestino() > v2.darIdDestino())
			return 1;
		else 
			return 0;

	}

}
