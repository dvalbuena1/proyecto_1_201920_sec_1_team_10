package model.logic;

import java.util.Comparator;

public class ComparadorTiempoPromedio implements Comparator<ViajeAbstracto> 
{

	@Override
	public int compare(ViajeAbstracto v1, ViajeAbstracto v2) {
		
		if(v1.darTiempoPromedio() < v2.darTiempoPromedio())
			return -1;
		else if(v1.darTiempoPromedio() > v2.darTiempoPromedio())
			return 1;
		else 
			return 0;
	}	

}
