package model.logic;
/**
 * 
 * 
 *
 */
public abstract class ViajeAbstracto
{
	/**
	 * 
	 */
	protected int idOrigen;
	/**
	 * 
	 */
	protected int idDestino;
	/**
	 * 
	 */
	protected double tiempoPromedio;
	/**
	 * 
	 */
	protected double desviacionEstandar;
	/**
	 * 
	 */
	protected double tiempoPromedioGeometrico;
	/**
	 * 
	 */
	protected double desviacionEstandarGeometrico;
	
	/**
	 * 
	 * @param pIdOrigen
	 * @param pIdDestino
	 * @param pTiempoPromedio
	 * @param pDesviacionEstandar
	 * @param pTiempoPromedioGeometrico
	 * @param pDesviacionEstandarGeometrico
	 */
	public ViajeAbstracto(int pIdOrigen, int pIdDestino, double pTiempoPromedio, double pDesviacionEstandar, double pTiempoPromedioGeometrico, double pDesviacionEstandarGeometrico)
	{
		idOrigen=pIdOrigen;
		idDestino=pIdDestino;
		tiempoPromedio=pTiempoPromedio;
		desviacionEstandar=pDesviacionEstandar;
		tiempoPromedioGeometrico=pTiempoPromedioGeometrico;
		desviacionEstandarGeometrico=pDesviacionEstandarGeometrico;
	}
	/**
	 * 
	 * @return
	 */
	public int darIdOrigen()
	{
		return idOrigen;
	}
	/**
	 * 
	 * @return
	 */
	public int darIdDestino()
	{
		return idDestino;
	}
	/**
	 * 
	 * @return
	 */
	public double darTiempoPromedio()
	{
		return tiempoPromedio;
	}
	/**
	 * 
	 * @return
	 */
	public double darDesviacionEstandar()
	{
		return desviacionEstandar;
	}
	/**
	 * 
	 * @return
	 */
	public double darTiempoPromedioGeometrico()
	{
		return tiempoPromedioGeometrico;
	}
	/**
	 * 
	 * @return
	 */
	public double darDesviacionEstandarGeometrico()
	{
		return desviacionEstandarGeometrico;
	}	
}
