package model.logic;

import java.util.Comparator;

public class Ordenamientos<E extends Comparable<E>> 
{
	//Metodo Para ordenar por ShellSort
	public void sortShell(Comparable<E>[] a)
	{
		int n = a.length;

        // 3x+1 increment sequence:  1, 4, 13, 40, 121, 364, 1093, ... 
        int h = 1;
        while (h < n/3) 
        	h = 3*h + 1; 

        while (h >= 1) 
        {
            // h-sort the array
            for (int i = h; i < n; i++) 
            {
                for (int j = i; j >= h && less(a[j], a[j-h]); j -= h) 
                {
                    exch(a, j, j-h);
                }
            }
            
            h /= 3;
        }		
	}
	
	//Metodo Para ordenar por MergeSort
	public void sortMerge(Comparable<E>[] a)
	{
		Comparable<E>[] aux = new Comparable[a.length];
		sortMerge2(a, aux, 0, a.length-1);		
	} 
	
	private void sortMerge2(Comparable<E>[] a, Comparable<E>[] aux, int lo, int hi) 
	{
		if (hi <= lo) 
			return;
		int mid = lo + (hi - lo) / 2;
		sortMerge2(a, aux, lo, mid);
		sortMerge2(a, aux, mid + 1, hi);
		merge(a, aux, lo, mid, hi);
		
	}

	public void merge(Comparable<E>[] a, Comparable<E>[] aux, int lo, int mid, int hi)
	{
		// precondition: a[lo .. mid] and a[mid+1 .. hi] are sorted subarrays


		// copy to aux[]
		for (int k = lo; k <= hi; k++) 
		{
			aux[k] = a[k]; 
		}

		// merge back to a[]
		int i = lo, j = mid+1;
		for (int k = lo; k <= hi; k++) 
		{
			if      (i > mid)              
				a[k] = aux[j++];
			else if (j > hi)              
				a[k] = aux[i++];
			else if (less(aux[j], aux[i]))
				a[k] = aux[j++];
			else                           
				a[k] = aux[i++];
		}

		// postcondition: a[lo .. hi] is sorted	


	}
	
	//Metodo Para ordenar por QuickSort 3-Way
	public void sortQuick(Comparable<E>[] arr, Comparator<E> compare, boolean up) 
	{		
	  sortQuick2(arr, compare, up, 0, arr.length - 1);		
	}
	
	private void sortQuick2(Comparable<E>[] arr, Comparator<E> compare, boolean up, int lo, int hi)
	{
		if (hi <= lo)
			return;
        int [] n = partition(arr, compare, up, lo, hi);
        sortQuick2(arr, compare, up, lo, n[0]-1);
        sortQuick2(arr, compare, up, n[1]+1, hi);
	}
	
	private int[] partition(Comparable<E>[] arr, Comparator<E> compare, boolean up, int lo, int hi)
	{
		int lt = lo;
		int gt = hi;
		int i = lo+1;
		
		int random = (int) (Math.random()*((hi - lo)+1)+lo);
		E v = (E) arr[random];
		exch(arr, lo, random);
		
		int cmp = 0;
		
		if(compare != null)
			if(up)
				cmp = compare.compare((E) arr[i], v);
			else
				cmp = compare.compare(v,(E) arr[i]);
				
		else
			if(up)
				cmp = arr[i].compareTo(v);
			else
				cmp = v.compareTo((E) arr[i]);
			
		
			
		while (i <= gt) 
		{ 
			if(cmp < 0)
				exch(arr, lt++, i++);
			else if(cmp > 0)
				exch(arr, i, gt--);
			else
				i++;			
		}

		return new int[] {lt,gt};

	}
	
	public static void main(String[] args) {
		
	}
	
	//-------------------------------------------------------
	// Metodo Auxiliares
	//-------------------------------------------------------
	
	private boolean less(Comparable<E> v, Comparable<E> w)
	{
		return v.compareTo((E) w)<0;
	}
	
	private  void exch(Object[] a, int i, int j)
	{
		E swap = (E) a[i];
		a[i] = a[j];
		a[j] = swap;		
	}


}
