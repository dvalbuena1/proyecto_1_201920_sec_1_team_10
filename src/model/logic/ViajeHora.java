package model.logic;
/**
 * 
 * 
 *
 */
public class ViajeHora extends ViajeAbstracto implements Comparable<ViajeHora>
{
	/**
	 * 
	 */
	private int horaDia;
	
	/**
	 * 
	 * @param pIdOrigen
	 * @param pIdDestino
	 * @param pHoraDia
	 * @param pTiempoPromedio
	 * @param pDesviacionEstandar
	 * @param pTiempoPromedioGeometrico
	 * @param pDesviacionEstandarGeometrico
	 */
	public ViajeHora(int pIdOrigen, int pIdDestino, int pHoraDia, double pTiempoPromedio, double pDesviacionEstandar, double pTiempoPromedioGeometrico, double pDesviacionEstandarGeometrico)
	{
		super(pIdOrigen, pIdDestino, pTiempoPromedio, pDesviacionEstandar, pTiempoPromedioGeometrico, pDesviacionEstandarGeometrico);
		horaDia=pHoraDia;
	}
	/**
	 * 
	 * @return
	 */
	public int darHoraDia()
	{
		return horaDia;
	}
	
	@Override
	public int compareTo(ViajeHora v) 
	{
		if(this.darHoraDia() < v.darHoraDia())
			return -1;
		else if(this.darHoraDia() > v.darHoraDia())
			return 1;
		else
			return 0;
	}
	
	
}
