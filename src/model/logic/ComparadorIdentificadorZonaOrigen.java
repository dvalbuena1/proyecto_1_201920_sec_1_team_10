package model.logic;

import java.util.Comparator;

public class ComparadorIdentificadorZonaOrigen implements Comparator<ViajeAbstracto> 
{

	@Override
	public int compare(ViajeAbstracto v1, ViajeAbstracto v2) 
	{
		if(v1.darIdOrigen() < v2.darIdOrigen())
			return -1;
		else if(v1.darIdOrigen() > v2.darIdOrigen())
			return 1;
		else 
			return 0;
	}
	
	
	
}
