package model.logic;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import com.opencsv.CSVReader;

import model.data_structures.ListaEncadenada;




/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo { 

	/**
	 * Atributo del modelo del mundo
	 */
	private ListaEncadenada<ViajeHora> listaHora;

	/**
	 * Atributos del modelo del mundo
	 */
	private ListaEncadenada<ViajeMes> listaMes;

	/**
	 * 
	 */
	private ListaEncadenada<ViajeSemana> listaSemana;
	
	/**
	 * 
	 */
	private Ordenamientos orden;
	
	/**
	 * 
	 */
	private int trimestre;

	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		listaMes = new ListaEncadenada<>();
		listaHora = new ListaEncadenada<>();
		listaSemana = new ListaEncadenada<>();
		orden = new Ordenamientos<>();
		trimestre = 0;
	}

	/**
	 * 
	 * @return
	 */
	public int sizeH()
	{
		return listaHora.size();
	}
	/**
	 * 
	 * @return
	 */
	public int sizeM()
	{
		return listaMes.size();
	}

	/**
	 * 
	 * @return
	 */
	public int sizeS()
	{
		return listaSemana.size();
	}
	
	/**
	 * 
	 * @return
	 */
	public int darTrimestre()
	{
		return trimestre;
	}

	/**
	 * 
	 * @param trimestre
	 */
	public void asignarTrismestre(int trimestre) 
	{
		this.trimestre = trimestre;		
	}
	
	/**
	 * 
	 * @param ruta
	 * @param i 
	 * @param trimestre 
	 * @throws IOException
	 */
	public void cargarArchivo(String ruta, int i) throws IOException 
	{
		CSVReader reader = new CSVReader(new FileReader(ruta));
		String[] datos = reader.readNext();
		while((datos = reader.readNext()) != null)
		{
			ViajeAbstracto viaje;
			int horaDia;
			int mes;
			int semana;
			int idOrigen = Integer.parseInt(datos[0]);
			int idDestino = Integer.parseInt(datos[1]);
			double tPromedio = Double.parseDouble(datos[3]);
			double dEstandar = Double.parseDouble(datos[4]); 
			double tPromedioG = Double.parseDouble(datos[5]);
			double dEstandarG = Double.parseDouble(datos[6]);
			if(i == 1)
			{
				horaDia = Integer.parseInt(datos[2]);
				viaje = new ViajeHora(idOrigen, idDestino, horaDia, tPromedio, dEstandar, tPromedioG, dEstandarG);
				listaHora.add((ViajeHora) viaje);
			}
			else if(i == 2)
			{
				mes = Integer.parseInt(datos[2]);
				viaje = new ViajeMes(idOrigen, idDestino, mes, tPromedio, dEstandar, tPromedioG, dEstandarG);				
				listaMes.add((ViajeMes) viaje);
			}
			else
			{
				semana = Integer.parseInt(datos[2]);
				viaje = new ViajeSemana(idOrigen, idDestino, semana, tPromedio, dEstandar, tPromedioG, dEstandarG);				
				listaSemana.add((ViajeSemana) viaje);
			}

		}	
		reader.close();
	} 

	/**
	 * 
	 * @return
	 */
	public int darMenorIdentificador()
	{
		int menor = Integer.MAX_VALUE;
		int i = 0;
		while(i < 3)
		{
			Iterator it;

			if(i == 0)
			{
				it = listaHora.iterator();
			}
			else if(i == 1)
			{
				it = listaMes.iterator();
			}
			else
			{
				it = listaSemana.iterator();
			}

			while(it.hasNext())
			{
				ViajeAbstracto viaje = (ViajeAbstracto) it.next();
				int men = viaje.darIdOrigen();
				if(men < menor)
					menor = men; 				
			}

			i++;
		}
		return menor;
	}

	/**
	 *  
	 * @return
	 */
	public int darMayorIdentificador()
	{
		int mayor = 0;

		int i = 0;
		while(i < 3)
		{
			Iterator it;

			if(i == 0)
			{
				it = listaHora.iterator();
			}
			else if(i == 1)
			{
				it = listaMes.iterator();
			}
			else
			{
				it = listaSemana.iterator();
			}
			while(it.hasNext())
			{
				ViajeAbstracto viaje = (ViajeAbstracto) it.next();
				int men = viaje.darIdOrigen();
				if(men > mayor)
					mayor = men; 				
			}

			i++;
		}

		return mayor;
	}

	/**
	 * Consultar el tiempo promedio de viaje y su desviación estándar de los viajes entre una
	 * zona de origen y una zona destino para un mes dado.
	 * @param zonaOri Zona de Origen
	 * @param zonaDes Zona de destino
	 * @param mes Mes
	 * @return Lista con los viajes que cumplen con los parametros dados.
	 */
	public ListaEncadenada<ViajeMes> darTiempoDesviacionMes(int zonaOri, int zonaDes, int mes)
	{
		//R2
		ListaEncadenada<ViajeMes> viajes = new ListaEncadenada<>();
		Iterator<ViajeMes> it = listaMes.iterator();
		
		while(it.hasNext())
		{
			ViajeMes viaje = it.next();
			
			int zonaOV = viaje.darIdOrigen();
			int zonaDV = viaje.darIdDestino();
			int mesV = viaje.darMes();
			
			if(zonaOV == zonaOri && zonaDV == zonaDes && mesV == mes)
				viajes.add(viaje);			
		}		
		return viajes;
	} 

	/**
	 * 
	 * @param mes
	 * @param cantidad
	 * @param compare
	 * @return
	 */
	public ViajeMes[] darNumeroViajesMesMayorPromedio(int mes, int cantidad)
	{
		//R3
		ListaEncadenada<ViajeMes> viajes = new ListaEncadenada<>();
		Iterator<ViajeMes> it = listaMes.iterator();
		
		while(it.hasNext())
		{
			ViajeMes viaje = it.next();
			
			int mesV = viaje.darMes();
			
			if(mesV == mes)
				viajes.add(viaje);			
		}
		Comparable<ViajeMes>[] ordenar = viajes.toArray();
		orden.sortQuick(ordenar, new ComparadorTiempoPromedio(), false);
		ViajeMes[] arrRetornar= new ViajeMes[cantidad];
		int i=0;
		while(i<cantidad)
		{
			ViajeMes viaje= (ViajeMes) ordenar[i];
			arrRetornar[i]=viaje;
			i++;
		}
		
		return arrRetornar;		
	}

	/**
	 * 
	 * @param mes
	 * @param zonaMenor
	 * @param zonaMayor
	 * @param compare
	 * @param zonaOrigen
	 * @return
	 */
	public Comparable<ViajeMes>[][] darComparacionTiempoPromedioMes(int mes, int zonaMenor, int zonaMayor, int zona)
	{
		//R4
		ListaEncadenada<ViajeMes> listaOrden1= new ListaEncadenada<>();
		ListaEncadenada<ViajeMes> listaOrden2= new ListaEncadenada<>();
		Comparable<ViajeMes>[][] ret = new Comparable[2][];
		Iterator<ViajeMes> it = listaMes.iterator();
		
		while(it.hasNext())
		{
			ViajeMes viaje= it.next();
			int zonaO= viaje.darIdOrigen();
			int mesV= viaje.darMes();
			int zonaD= viaje.darIdDestino();
			if(mes==mesV && zonaO==zona && (zonaD>=zonaMenor || zonaD<=zonaMayor))
			{
				listaOrden1.add(viaje);
				
			}
			if(mes==mesV && zonaD==zona && (zonaO>=zonaMenor || zonaO<=zonaMayor))
			{
				listaOrden2.add(viaje);
			}			 
		}
		
		Comparable<ViajeMes>[] arr1= listaOrden1.toArray();
		orden.sortQuick(arr1, new ComparadorIdentificadorZonaDestino(), true);
		Comparable<ViajeMes>[] arr2= listaOrden2.toArray();
		orden.sortQuick(arr2, new ComparadorIdentificadorZonaOrigen(), true);
		ret[0] = arr1;
		ret[1] = arr2;
		
		return ret;		
	}

	/**
	 * 
	 * @param zonaOr
	 * @param zonaDes
	 * @param dia
	 * @return
	 */
	public ListaEncadenada<ViajeSemana> darTiempoDesviacionSemana(int zonaOr, int zonaDes, int dia)
	{
		//R5
		ListaEncadenada<ViajeSemana> lista = new ListaEncadenada<ViajeSemana>();
		Iterator<ViajeSemana> it= listaSemana.iterator();
		
		while(it.hasNext())
		{
			ViajeSemana viaje= it.next();
			int zonaOri= viaje.darIdOrigen();
			int zonaDest= viaje.darIdDestino();
			int diaSemana= viaje.darDiaSemana();
			
			if(zonaOri==zonaOr && zonaDest==zonaDes && diaSemana==dia)
			{
				lista.add(viaje);
			}
			
			
			
		}
		return lista;
	
	}

	/**
	 * 
	 * @param dia
	 * @param numero
	 * @param compare
	 * @return
	 */
	public ViajeSemana[] darNumeroViajesSemanaMayorPromedio(int dia, int numero)
	{
		//R6
		
		ListaEncadenada<ViajeSemana> lista= new ListaEncadenada<ViajeSemana>();
		Iterator<ViajeSemana> it = listaSemana.iterator();
		
		while(it.hasNext())
		{
			ViajeSemana viaje= it.next();
			int diaSemana= viaje.darDiaSemana();
			if(diaSemana==dia)
			{
				lista.add(viaje);
			}
			
		}
		Comparable<ViajeSemana>[] listaOrden= lista.toArray();
		orden.sortQuick(listaOrden, new ComparadorTiempoPromedio(), false);
		ViajeSemana[] arrRetornar= new ViajeSemana[numero];
		int i=0;
		while(i<numero)
		{
			ViajeSemana viaje= (ViajeSemana) listaOrden[i];
			arrRetornar[i]=viaje;
			i++;
		}
		
		return arrRetornar;		
	}

	/**
	 * 
	 * @param mes
	 * @param zonaMenor
	 * @param zonaMayor
	 * @param compare
	 * @param zonaOrigen
	 * @return
	 */
	public Comparable<ViajeSemana>[][] darComparacionTiempoPromedioSemana(int diaSemana, int zonaMenor, int zonaMayor, int zona)
	{
		//R7
		ListaEncadenada<ViajeSemana> listaOrden1= new ListaEncadenada<>();
		ListaEncadenada<ViajeSemana> listaOrden2= new ListaEncadenada<>();
		Comparable<ViajeSemana>[][] ret= new ViajeSemana[2][];
		Iterator<ViajeSemana> it = listaSemana.iterator();
		
		while(it.hasNext())
		{
			ViajeSemana viaje= it.next();
			int zonaO= viaje.darIdOrigen();
			int dia= viaje.darDiaSemana();
			int zonaD= viaje.darIdDestino();
			if(diaSemana==dia && zonaO==zona && (zonaD>=zonaMenor || zonaD<=zonaMayor))
			{
				listaOrden1.add(viaje);
				
			}
			if(diaSemana==dia && zonaD==zona && (zonaO>=zonaMenor || zonaO<=zonaMayor))
			{
				listaOrden2.add(viaje);
			}			
		}
		
		Comparable<ViajeSemana>[] arr1= listaOrden1.toArray();
		orden.sortQuick(arr1, new ComparadorIdentificadorZonaDestino(), true);
		Comparable<ViajeSemana>[] arr2= listaOrden2.toArray();
		orden.sortQuick(arr2, new ComparadorIdentificadorZonaOrigen(), true);
		ret[0] = arr1;
		ret[1] = arr2;
		
		return ret;			
	}

	/**
	 * 
	 * @param horaInicial
	 * @param horaFinal
	 * @param zonaOr
	 * @param zonaDes
	 * @return
	 */
	public ListaEncadenada<ViajeHora> darViajesFranjaHoraria(int horaInicial, int horaFinal, int zonaOr, int zonaDes)
	{
		//R8
		ListaEncadenada<ViajeHora> lista = new ListaEncadenada<ViajeHora>();
		Iterator<ViajeHora> it= listaHora.iterator();
		
		while(it.hasNext())
		{
			ViajeHora viaje= it.next();
			int hora= viaje.darHoraDia();
			int origen= viaje.darIdOrigen();
			int destino= viaje.darIdDestino();
			
			if(origen==zonaOr && destino==zonaDes && hora>=horaInicial && hora<=horaFinal)
			{
				lista.add(viaje);
			}			
		}
		
		lista.clear();		
		Comparable<ViajeHora>[] listaVH = lista.toArray();
		orden.sortQuick(listaVH, null, true);
		
		for (Comparable<ViajeHora> viajeHora : listaVH) 
			lista.add((ViajeHora)viajeHora);	
		
		return lista;		
	}

	/**
	 * 
	 * @param hora
	 * @param numero
	 * @param compare
	 * @return
	 */
	public ViajeHora[] darNumeroViajesHoraMayorPromedio(int hora, int numero)
	{
		//R9
		ListaEncadenada<ViajeHora> lista= new ListaEncadenada<ViajeHora>();
		Iterator<ViajeHora> it = listaHora.iterator();
		
		while(it.hasNext())
		{
			ViajeHora viaje= it.next();
			int horaConsultar=viaje.darHoraDia();
			if(horaConsultar==hora)
			{
				lista.add(viaje);
			}
			
		}
		Comparable<ViajeHora>[] listaOrden= lista.toArray();
		orden.sortQuick(listaOrden, new ComparadorTiempoPromedio(), false);
		ViajeHora[] arrRetornar= new ViajeHora[numero];
		int i=0;
		while(i<numero)
		{
			ViajeHora viaje= (ViajeHora) listaOrden[i];
			arrRetornar[i]=viaje;
			i++;
		}
		
		return arrRetornar;
	}

	/**
	 *  
	 * @param zonaOr
	 * @param zonaDes
	 * @return
	 */
	public int[] darListaParaGraficaASCII(int zonaOr, int zonaDes)
	{
		//R10
		ListaEncadenada<ViajeHora> lista = new ListaEncadenada<ViajeHora>();
		Iterator<ViajeHora> it = listaHora.iterator();
		
		while(it.hasNext())
		{
			ViajeHora viaje= it.next();
			int origen= viaje.darIdOrigen();
			int destino= viaje.darIdDestino();
			if(origen==zonaOr&& destino==zonaDes)
			{
				lista.add(viaje);				
			}
		}
		
		Comparable<ViajeHora>[] ordenar = lista.toArray();
		orden.sortQuick(ordenar, null, true);
		
		int[] horas = new int[24];		
		int hora = 0;
		double promedio = 0;
		for (int i = 0; i < ordenar.length; i++) 
		{
			int horaDia = ((ViajeHora)ordenar[i]).darHoraDia();
			double pPromedio = ((ViajeHora)ordenar[i]).darTiempoPromedio();
			if(hora == horaDia)
				promedio += pPromedio;
			else
			{
				promedio = darAproximacionMinuto(promedio);
				horas[hora] = (int) promedio;
				hora = horaDia;
				promedio = 0;
				i--;
			}				
		}		
		return horas;		
	}

	/**
	 * 
	 * @param segundos
	 * @return
	 */
	public int darAproximacionMinuto(double segundos)
	{		
		//Metodo que ayuda al requerimiento 10
		double minutos= segundos/60;
		return (int) Math.round(minutos);		
	}

}
	
	