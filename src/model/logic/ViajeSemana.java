package model.logic;
/**
 * 
 * 
 *
 */
public class ViajeSemana extends ViajeAbstracto implements Comparable<ViajeSemana>
{
	/**
	 * 
	 */
	private int diaSemana;
	/**
	 * 
	 * @param pIdOrigen
	 * @param pIdDestino
	 * @param pTiempoPromedio
	 * @param pDesviacionEstandar
	 * @param pTiempoPromedioGeometrico
	 * @param pDesviacionEstandarGeometrico
	 */
	public ViajeSemana(int pIdOrigen, int pIdDestino, int pDiaSemana, double pTiempoPromedio, double pDesviacionEstandar, double pTiempoPromedioGeometrico, double pDesviacionEstandarGeometrico)
	{
		
		super(pIdOrigen, pIdDestino, pTiempoPromedio, pDesviacionEstandar, pTiempoPromedioGeometrico, pDesviacionEstandarGeometrico);
		diaSemana=pDiaSemana;
	}
	/**
	 * 
	 * @return
	 */
	public int darDiaSemana()
	{
		return diaSemana;
	}
	
	@Override
	public int compareTo(ViajeSemana v) 
	{
		if(this.darDiaSemana() < v.darDiaSemana())
			return -1;
		else if(this.darDiaSemana() > v.darDiaSemana())
			return 1;
		else
			return 0;
	}
}
