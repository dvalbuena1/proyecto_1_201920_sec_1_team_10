package model.logic;
/**
 * Clase de los viajes organizados por mes. *
 */
public class ViajeMes extends ViajeAbstracto implements Comparable<ViajeMes>
{
	/**
	 * Mes del viaje
	 */
	private int mes;
	
	/**
	 * Inicializa los atributos con los datos dados por parametro
	 * @param pSourceId Dato con el id de la zona de origen del viaje.
	 * @param pDestinoId Dato con el id de la zona de destino del viaje.
	 * @param pMeanTime Dato con el tiempo promedio del viaje.
	 * @param pStandardTime Dato con la desviacion estandar.
	 * @param pGeometricMean Dato con el tiempo promedio geometrico 
	 * @param pGeometricStandard Dato con la desviacion estandar geometrica
	 * @param pMes Dato con el mes del viaje
	 */
	public ViajeMes(int pSourceId, int pDestinoId, int pMes, double pMeanTime, double pStandardTime, double pGeometricMean,
			double pGeometricStandard) 
	{
		super(pSourceId, pDestinoId, pMeanTime, pStandardTime, pGeometricMean, pGeometricStandard);
		mes=pMes;
		
	}
	/**
	 * Da el mes del viaje
	 * @return mes del viaje
	 */
	public int darMes()
	{
		return mes;
	}
	
	@Override
	public int compareTo(ViajeMes v) 
	{		
		if(this.darMes() < v.darMes())
			return -1;
		else if(this.darMes() > v.darMes())
			return 1;
		else
			return 0;
	}

}
